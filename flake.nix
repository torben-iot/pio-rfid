{
  description = "VSCode Environment for PlatformIO";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; config.allowUnfree = true; };
        targetPkgs = p: with p; [
          gcc
          glibc
          vscode
          zlib
          (python311.withPackages (p: with p; [ setuptools ]))
          platformio
          avrdude
          esptool
          (vscode-with-extensions.override {
            vscodeExtensions = with vscode-extensions; [
              ms-vscode.cpptools
              bbenoist.nix
              ms-vscode-remote.remote-containers
            ] ++ p.vscode-utils.extensionsFromVscodeMarketplace [
              {
                name = "platformio-ide";
                publisher = "PlatformIO";
                version = "3.1.1";
                sha256 = "sha256-g9yTG3DjVUS2w9eHGAai5LoIfEGus+FPhqDnCi4e90Q=";
              }
            ];
          })
        ];

      in
      rec {
        formatter = nixpkgs.legacyPackages.${system}.nixpkgs-fmt;
        devShell = (pkgs.buildFHSUserEnv {
          name = "VSCode Environment for PlatformIO";
          inherit targetPkgs;
          runScript = pkgs.writeScript "init.sh" ''
            echo 'Hi there :)'
            echo Useful commands:
            echo '`pio run` - process/build project from the current directory'
            echo '`pio run --target upload` or `pio run -t upload` - upload firmware to embedded board'
            echo '`pio run --target clean` - clean project (remove compiled files)'
            echo '`pio device monitor` - open serial monitor'
          '';
        }).env;

        packages = {
          upload = pkgs.writeShellScriptBin "upload" ''
            ${pkgs.platformio}/bin/pio run --target upload
          '';

          monitor = pkgs.writeShellScriptBin "monitor" ''
            ${pkgs.platformio}/bin/pio device monitor
          '';
        };
      });
}
