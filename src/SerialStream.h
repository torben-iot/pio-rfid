#pragma once

#include <ostream>
#include <MFRC522.h>

class SerialStream {
public:
  SerialStream& operator<<(const char* str);
  SerialStream& operator<<(const String& str);
  SerialStream& operator<<(const __FlashStringHelper* str);
  SerialStream& operator<<(const MFRC522::Uid& uid);
  SerialStream& operator<<(std::ostream& (*pf)(std::ostream&));
  SerialStream& operator<<(const MFRC522& rfid);
};

extern SerialStream serial;
