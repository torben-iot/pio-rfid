#include "SerialStream.h"

SerialStream& SerialStream::operator<<(const char* str) {
  Serial.print(str);
  return *this;
}

SerialStream& SerialStream::operator<<(const String& str) {
  Serial.print(str);
  return *this;
}

SerialStream& SerialStream::operator<<(const __FlashStringHelper* str) {
  Serial.print(str);
  return *this;
}

SerialStream& SerialStream::operator<<(const MFRC522::Uid& uid) {
  for (uint8_t i = 0; i < uid.size; ++i) {
    Serial.print(uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(uid.uidByte[i], HEX);
  }
  return *this;
}

SerialStream& SerialStream::operator<<(std::ostream& (*pf)(std::ostream&)) {
  Serial.println();
  return *this;
}

SerialStream& SerialStream::operator<<(const MFRC522& rfid) {
  return *this << F("Card UID:")
              << rfid.uid
              << F("\nPICC Type: ")
              << rfid.PICC_GetTypeName(rfid.PICC_GetType(rfid.uid.sak))
              << std::endl;
}

SerialStream serial;
