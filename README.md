# RFID Reader Project
This project is a simple RFID reader using the MFRC522 module and ESP32. The reader scans for RFID tags, reads their UIDs and PICC types, and prints this information to the serial monitor.

The project uses PlatformIO for building and uploading the code to the ESP32. It also includes a custom SerialStream class for easy printing to the serial monitor.

## Usage
The project includes two Nix packages for uploading the code to the ESP32 and monitoring the serial output:

- **Upload**: To upload the code to the ESP32, run the following command:
```bash
nix run .#upload
```

- **Monitor**: To view the serial output from the ESP32, run the following command:
```bash
nix run .#monitor
```

To run these commands without cloning, use
```bash
nix run gitlab:torben-iot/pio-rfid#upload
```
resp.
```bash
nix run gitlab:torben-iot/pio-rfid#monitor
```

## Code Overview
The main code for the project is in main.cpp. It initializes the MFRC522 RFID reader and enters a loop where it continuously checks for new RFID tags. When a new tag is detected, it prints the tag’s UID and PICC type to the serial monitor.

The SerialStream class is used to simplify printing to the serial monitor. It overloads the << operator so you can use it just like cout in C++.

## Dependencies
This project depends on the following libraries:

- SPI.h: This is the built-in SPI library for Arduino.
- MFRC522.h: This is the library for the MFRC522 RFID module.

## More Information
For more information about the MFRC522 RFID module and how to use it with Arduino, check out this tutorial: Arduino RFID NFC Tutorial.
